-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 17 Sep 2019 pada 18.50
-- Versi server: 10.1.38-MariaDB
-- Versi PHP: 7.1.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sistem_pakar`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `history`
--

CREATE TABLE `history` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` char(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `petugas` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kesimpulan` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `history`
--

INSERT INTO `history` (`id`, `nama`, `status`, `petugas`, `kesimpulan`, `created_at`, `updated_at`) VALUES
(1, 'Atang Sanjaya', 'DITOLAK', 'Thamrin Siregar', 'a:1:{i:0;s:86:\"Kain tersebut masuk standar dan simpan pada “layout passed” yang telah disediakan.\";}', '2019-09-13 01:53:21', '2019-09-13 01:53:21'),
(2, 'Atang Sanjaya', 'DISETUJUI', '', 'Kain tersebut masuk standar dan simpan pada “layout passed” yang telah disediakan.', '2019-09-13 02:12:11', '2019-09-13 02:12:11'),
(3, 'Denaldi Eza Nuari', 'TIDAK SELESAI', 'Belum diproses petugas', 'BELUM ADA', '2019-09-17 16:18:52', '2019-09-17 16:18:52');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jabatan`
--

CREATE TABLE `jabatan` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `jabatan`
--

INSERT INTO `jabatan` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'section head', '2019-09-12 11:48:27', '2019-09-12 11:48:27'),
(2, 'group leader', '2019-09-12 11:48:27', '2019-09-12 11:48:27'),
(3, 'operator', '2019-09-12 11:48:28', '2019-09-12 11:48:28'),
(4, 'admin', '2019-09-12 11:48:28', '2019-09-12 11:48:28');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jabatan_user`
--

CREATE TABLE `jabatan_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `jabatan_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `jabatan_user`
--

INSERT INTO `jabatan_user` (`id`, `jabatan_id`, `user_id`) VALUES
(1, 3, 3),
(2, 4, 4),
(3, 3, 5),
(4, 3, 6),
(5, 3, 7),
(6, 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `jawaban`
--

CREATE TABLE `jawaban` (
  `id` int(11) NOT NULL,
  `nama_jawaban` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jawaban`
--

INSERT INTO `jawaban` (`id`, `nama_jawaban`) VALUES
(0, 'TIDAK'),
(1, 'YA');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori`
--

CREATE TABLE `kategori` (
  `id` int(11) NOT NULL,
  `nama_kategori` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kategori`
--

INSERT INTO `kategori` (`id`, `nama_kategori`) VALUES
(0, 'Warna dan Corak'),
(1, 'Benang'),
(2, 'Kualitas'),
(3, 'Konstruksi pada Benang');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_07_02_074619_history', 1),
(4, '2019_09_12_172314_create_roles_table', 1),
(5, '2019_09_12_172714_create_role_user_table', 1),
(6, '2019_09_12_175156_create_jabatan_user_table', 1),
(7, '2019_09_12_175900_create_jabatan_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pertanyaan`
--

CREATE TABLE `pertanyaan` (
  `id` int(11) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `jawaban_id` int(11) NOT NULL,
  `solusi` varchar(255) NOT NULL,
  `kategori_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pertanyaan`
--

INSERT INTO `pertanyaan` (`id`, `deskripsi`, `jawaban_id`, `solusi`, `kategori_id`) VALUES
(0, 'Apakah warna kain belang?', 0, 'Diinformasikan ke Purchasing dan Penyedia Eksternal agar proses pencelupan harus diperhatikan. Simpan barang tersebut di layout Reject', 0),
(1, 'Apakah serat kain putus?', 0, 'Diinformasikan ke Purchasing dan Penyedia Eksternal agar proses rajut atau tenun harus diperhatikan. Simpan barang tersebut di layout Reject', 1),
(2, 'Apakah kain kotor dan bernoda?', 0, 'Diinformasikan ke Purchasing dan Penyedia Eksternal agar proses washing dan lingkungan kerja harus diperhatikan. Simpan barang tersebut di layout Reject', 2),
(3, 'Apakah serat pada kain timbul atau longgar?', 0, 'Diinformasikan ke Purchasing dan Penyedia Eksternal agar proses rajut atau tenun tension benang harus diperhatikan. Simpan barang tersebut di layout Reject.', 3),
(4, 'Apakah hasil printing pada kain tidak merata?', 0, 'Diinformasikan ke Purchasing dan Penyedia Eksternal agar proses printing diperhatikan', 0),
(5, 'Apakah terdapat cacat printing pada kain?', 0, 'Diinformasikan ke Purchasing dan Penyedia Eksternal agar proses pencelupan harus diperhatikan. Simpan barang tersebut di layout Reject', 0),
(6, 'Apakah serat pada kain menggaris ke arah\r\npanjang ataupun lebar?', 0, 'Diinformasikan ke Purchasing dan Penyedia Eksternal agar proses rajut atau tenun harus diperhatikan. Simpan barang tersebut di layout Reject', 1),
(7, 'Apakah serat kain putus dengan panjang yang bervariatif?', 0, 'Diinformasikan ke Purchasing dan Penyedia Eksternal agar proses rajut atau tenun harus diperhatikan. Simpan barang tersebut di layout Reject', 1),
(8, 'Apakah kain terdapat kotor atau noda yang tidak dapat dibersihkan?', 0, 'Diinformasikan ke Purchasing dan Penyedia Eksternal agar proses washing dan lingkungan kerja harus diperhatikan. Simpan barang tersebut di layout Reject', 2),
(9, 'Apakah kain memiliki bentuk kotor atau noda yang jelas?', 0, 'Diinformasikan ke Purchasing dan Penyedia Eksternal agar proses washing dan lingkungan kerja harus diperhatikan. Simpan barang tersebut di layout Reject', 2),
(10, 'Apakah serat kain menggumpal dan menumpuk?', 0, 'Diinformasikan ke Purchasing dan Penyedia Eksternal agar proses rajut atau tenun tension benang harus diperhatikan. Simpan barang tersebut di layout Reject.', 3),
(11, 'Apakah serat kain tidak tersusun sempurna dan longgar?', 0, 'Diinformasikan ke Purchasing dan Penyedia Eksternal agar proses rajut atau tenun tension benang harus diperhatikan. Simpan barang tersebut di layout Reject.', 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'An Admin', '2019-09-12 11:48:27', '2019-09-12 11:48:27'),
(2, 'employee', 'An Employee', '2019-09-12 11:48:27', '2019-09-12 11:48:27');

-- --------------------------------------------------------

--
-- Struktur dari tabel `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`) VALUES
(1, 2, 1),
(2, 2, 2),
(3, 2, 3),
(4, 1, 4),
(5, 2, 5),
(6, 2, 6),
(7, 2, 7),
(8, 1, 8),
(9, 1, 9);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jabatan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_scan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nik` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `jabatan`, `no_scan`, `nik`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Thamrin Siregar', 'thamrinsiregar@google.com', 'section head', '13008', '11081389', NULL, '$2y$10$maPf8ZgbKJReuQCr4vykW.l3Jxd.i9LTimrhR.tizut.FTdtGGaSm', NULL, '2019-09-12 11:48:28', '2019-09-12 11:48:28'),
(2, 'Atang Sanjaya', 'atang@google.com', 'Operator', '243342342342', '09930056', NULL, '$2y$10$QUUzpld2DSbMAH6ccXiTgO/sWVH54IXRr.3/U3ecpFr0bvkYVszKG', NULL, '2019-09-12 11:48:28', '2019-09-12 11:48:28'),
(3, 'Amir', 'amir@google.com', 'Operator', '13011', '04111696', NULL, '$2y$10$LvLH9nnHmtzr7QRbxAgzkOLEkw4ayFmvevB5/mWHxoi40d4E0aOf2', NULL, '2019-09-12 11:48:28', '2019-09-12 11:48:28'),
(4, 'Riswan', 'adminriswan@google.com', 'Admin', '13016', '08132665', NULL, '$2y$10$JdSfY0rvly3JPL0wZqAEv.JhpgU55fxC/zfD5ceT7M0gTGtOs8H6y', NULL, '2019-09-12 11:48:29', '2019-09-12 11:48:29'),
(5, 'Denaldi Eza Nuari', 'denaldi@google.com', 'Operator', '115185', 'P.15.05.01938', NULL, '$2y$10$PFIx455nDkFQki9Fm2lATeIP6A146Ai4PYSJL6IlgEvCykp2EYEiy', NULL, '2019-09-13 11:41:31', '2019-09-13 11:41:31'),
(6, 'Dennis Yogi', 'denis@google.com', 'Operator', '13107', 'P.13.07.01305', NULL, '$2y$10$CNuNv/EIgGSRInokspxPROYGHGshDY5t2dCUcSf0lbFFOvnZ1uvXS', NULL, '2019-09-13 11:46:49', '2019-09-13 11:46:49'),
(7, 'Ramdani', 'ramdani@google.com', 'Operator', '13106', 'P.13.07.01307', NULL, '$2y$10$fS5A5chrXbtql4n4GmFu0OfKEY78ItjcXzYrQtwyM5nnlupVX58Ie', NULL, '2019-09-13 11:51:04', '2019-09-13 11:51:04'),
(8, 'Adi', 'adi@google.com', 'Admin', '213131312', '12314141', NULL, '$2y$10$6B0CW3lbhW3/baWly5HFVOR0eeOy5ukQA3Vfp/0ojt9LarYJySxq2', NULL, '2019-09-15 01:40:43', '2019-09-15 01:40:43'),
(9, 'Testing', 'testing@google.com', 'section_head', '24124141', '2311314141', NULL, '$2y$10$/K/.kpSyEyYEKfoyxp3oXelVRq1j/X7JTde6X1o8udg26sqy3emP6', NULL, '2019-09-17 08:29:40', '2019-09-17 08:29:40');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `jabatan_user`
--
ALTER TABLE `jabatan_user`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `jawaban`
--
ALTER TABLE `jawaban`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pertanyaan`
--
ALTER TABLE `pertanyaan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kategori_pertanyaan` (`kategori_id`),
  ADD KEY `jawaban_pertanyaan` (`jawaban_id`);

--
-- Indeks untuk tabel `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `history`
--
ALTER TABLE `history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `jabatan`
--
ALTER TABLE `jabatan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `jabatan_user`
--
ALTER TABLE `jabatan_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `pertanyaan`
--
ALTER TABLE `pertanyaan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `pertanyaan`
--
ALTER TABLE `pertanyaan`
  ADD CONSTRAINT `jawaban_pertanyaan` FOREIGN KEY (`jawaban_id`) REFERENCES `jawaban` (`id`),
  ADD CONSTRAINT `kategori_pertanyaan` FOREIGN KEY (`kategori_id`) REFERENCES `kategori` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
