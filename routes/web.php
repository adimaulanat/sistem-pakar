<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function () {
    return view('auth.login');
});
Route::get('/beranda', function () {
    return view('beranda');
});
Route::Get('/contact', function () {
    return view('contact');
});
Route::get('/konsultasi', function () {
    return view('konsultasi.home');
});
Route::get('/konsultasi/pertanyaan/{id}', 'PertanyaanUserController@show');
Route::get('/konsultasi/solusi', 'PertanyaanUserController@kesimpulan');
Route::post('/konsultasi/history', 'PertanyaanUserController@history');
Route::get('/konsultasi/pertanyaan', 'PertanyaanUserController@pertanyaan');
// Route::get('/konsultasi/pertanyaan', function () {
//     return view('konsultasi.pertanyaan');
// });
Route::get('/tentang', function () {
    return view('tentang');
});
Route::get('/bantuan', function () {
    return view('bantuan');
});
Route::get('/dashboard', function () {
    return view('dashboard');
});
Route::get('/pengetahuan', 'PertanyaanController@index');
Route::post('/pengetahuan/store', 'PertanyaanController@store');
Route::post('/pengetahuan/update', 'PertanyaanController@postUpdate');
Route::get('/pengetahuan/delete/{id}', 'PertanyaanController@delete');
Route::get('/update', function () {
    return view('update');
});
Route::get('/update/{id}', 'PertanyaanController@update');
Route::get('/relasi', function () {
    return view('relasi');
});
Route::get('/standar', function () {
    return view('standar');
});

Route::get('/pegawai', 'PegawaiController@index');
Route::post('/pegawai/store', 'PegawaiController@store');
Route::get('/pegawai/update/{id}', 'PegawaiController@update');
Route::get('/pegawai/update', function () {
    return view('pegawai.update');
});
Route::post('/pegawai/update', 'PegawaiController@postUpdate');
Route::get('/pegawai/delete/{id}', 'PegawaiController@delete');

// Route CRUD
Route::get('/pertanyaan', 'PertanyaanController@index');
Route::get('/history', 'HistoryController@index');
Route::get('/history/approve/{id}/{name}', 'HistoryController@approve');
Route::get('/history/reject/{id}/{name}', 'HistoryController@reject');
Route::get('/history/export', 'HistoryController@export');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
