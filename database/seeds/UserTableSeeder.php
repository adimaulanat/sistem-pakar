<?php

use App\Jabatan;
use App\Role;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_employee = Role::where('name', 'employee')->first();
        $role_admin  = Role::where('name', 'admin')->first();

        $jabatan_section_head = Jabatan::where('name', 'section_head')->first();
        $jabatan_group_leader = Jabatan::where('name', 'group_leader')->first();
        $jabatan_operator = Jabatan::where('name', 'operator')->first();
        $jabatan_admin = Jabatan::where('name', 'admin')->first();

        $employee_section_head = new User();
        $employee_section_head->name = 'Thamrin Siregar';
        $employee_section_head->email = 'thamrinsiregar@google.com';
        $employee_section_head->password = Hash::make('password');
        $employee_section_head->no_scan = '13008';
        $employee_section_head->nik = '11081389';
        $employee_section_head->save();
        $employee_section_head->roles()->attach($role_employee);
        $employee_section_head->jabatans()->attach($jabatan_section_head);

        $employee_group_leader = new User();
        $employee_group_leader->name = 'Atang Sanjaya';
        $employee_group_leader->email = 'atang@google.com';
        $employee_group_leader->password = Hash::make('password');
        $employee_group_leader->no_scan = '243';
        $employee_group_leader->nik = '09930056';
        $employee_group_leader->save();
        $employee_group_leader->roles()->attach($role_employee);
        $employee_group_leader->jabatans()->attach($jabatan_group_leader);

        $employee_operator = new User();
        $employee_operator->name = 'Amir';
        $employee_operator->email = 'amir@google.com';
        $employee_operator->password = Hash::make('password');
        $employee_operator->no_scan = '13011';
        $employee_operator->nik = '04111696';
        $employee_operator->save();
        $employee_operator->roles()->attach($role_employee);
        $employee_operator->jabatans()->attach($jabatan_operator);

        $admin = new User();
        $admin->name = 'Riswan';
        $admin->email = 'adminriswan@google.com';
        $admin->password = Hash::make('password');
        $admin->no_scan = '13016';
        $admin->nik = '08132665';
        $admin->save();
        $admin->roles()->attach($role_admin);
        $admin->jabatans()->attach($jabatan_admin);
    }
}
