<?php

use App\Jabatan;
use Illuminate\Database\Seeder;

class JabatanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jabatan_section_head = new Jabatan();
        $jabatan_section_head->name = 'section head';
        $jabatan_section_head->save();

        $jabatan_group_leader = new Jabatan();
        $jabatan_group_leader->name = 'group leader';
        $jabatan_group_leader->save();

        $jabatan_operator = new Jabatan();
        $jabatan_operator->name = 'operator';
        $jabatan_operator->save();
        
        $jabatan_admin = new Jabatan();
        $jabatan_admin->name = 'admin';
        $jabatan_admin->save();
    }
}
