@extends ('layouts.layout')

@section ('content')
<div class="col-md-12" style="padding:20px;">
    @foreach($pertanyaan as $p)
    <form class="form-horizontal" action="/pengetahuan/update" method="post">
        {{ csrf_field() }}
        <!-- <input type="hidden" name="_token" value="{{ csrf_token() }}"> -->
        <input type="hidden" name="id" value="{{ $p->id }}">
        <div class="form-group">
            <label class="control-label col-sm-2" for="pertanyaan">Pertanyaan:</label>
            <div class="col-sm-10">
                <input type="pertanyaan" class="form-control" id="pertanyaan" name="pertanyaan" value="{{ $p->deskripsi }}">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="jawaban">Jawaban:</label>
            <div class="col-sm-10">
                <div class="checkbox">
                    <input {{ ( $p->jawaban_id == 1 ) ? 'checked' : '' }} type="radio" name="jawaban" value="1"> YA<br>
                    <input {{ ( $p->jawaban_id == 0 ) ? 'checked' : '' }} type="radio" name="jawaban" value="0"> TIDAK<br>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="solusi">Solusi:</label>
            <div class="col-sm-10">
                <input type="solusi" class="form-control" id="solusi" name="solusi" value="{{ $p->solusi }}">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="kategori">Kategori:</label>
            <div class="col-sm-10">
                <select name="kategori_id">
                    @foreach($kategori as $k)
                    <option {{ ( $p->kategori_id == $k->id ) ? 'selected' : '' }} name="kategori_id" value="{{$k->id}}">{{ $k->nama_kategori }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Submit</button>
            </div>
        </div>
    </form>
    @endforeach
</div>
@endsection