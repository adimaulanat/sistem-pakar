@extends ('layouts.layout')

@section ('content')
<!DOCTYPE html>
<html lang="en">

<head>
  <title>Bootstrap Example</title>
  <!-- <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script> -->
</head>
@if (session()->has('flash_notification.success'))
<div class="alert alert-success">{!! session('flash_notification.success') !!}</div>
@endif

<body>
  <div class="row">
    <div class="col-lg-6">
      <div class="text-container">
        <h1><span class="turquoise">Sistem Pakar</span></h1>
        <h2>Selamat Datang di Aplikasi Quality Control Menggunakan Metode Forward Chaining</p>
          <div class="container" style="margin-top: 30px;">
            <div class="row">
              <div class="col-sm-6">
                <h1 class="turquoise">Pegawai</h1>
                <h4>Menu untuk admin mengubah, menambah, ataupun menghapus data untuk pegawai yang terdaftar</p>
              </div>
              <div class="col-sm-6">
                <h1 class="turquoise">Pengetahuan</h1>
                <h4>Menu dimana admin mengatur mulai dari mengubah, menambah, ataupun menghapus data dari pertanyaan dan jawaban yang akan ditampilkan/p>
              </div>
            </div>
          </div>
      </div> <!-- end of text-container -->
    </div> <!-- end of col -->
    <div class="col-lg-6">
      <div class="image-container">
        <img class="img-fluid" src="{{asset('asset/img/header-teamwork.svg')}}" alt="alternative">
      </div> <!-- end of image-container -->
    </div> <!-- end of col -->
  </div> <!-- end of row -->
</body>

</html>

@endsection