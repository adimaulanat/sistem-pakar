@extends ('layouts.layout')

@section ('content')
<div class="col-md-12" style="padding:20px;">
    <br />
    <br />
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <form class="form-horizontal" action="/konsultasi/history" method="post" style="padding: 20px">
                        {{ csrf_field() }}
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button type="submit" class="btn btn-primary btn-lg"> Isi Pertanyaan </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection