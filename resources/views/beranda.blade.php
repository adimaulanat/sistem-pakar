<!-- @extends ('layouts.layout') -->

@section ('content')

<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Website Title -->
  <title>Evolo - StartUp HTML Landing Page Template</title>
  <!-- Favicon  -->
  <link rel="icon" href="images/favicon.png">
</head>

@if (session()->has('flash_notification.success'))
<div class="alert alert-success">{!! session('flash_notification.success') !!}</div>
@endif

@if(strtolower(Auth::user()->jabatan) == 'section head')
@if (session()->has('flash_notification.history'))
<div class="alert alert-warning">{!! session('flash_notification.history') !!}</div>
@endif
@endif

<div class="row">
  <div class="col-lg-6">
    <div class="text-container">
      <h1><span class="turquoise">Sistem Pakar</span></h1>
      <h4>Selamat Datang di Aplikasi Quality Control Menggunakan Metode Forward Chaining</h4>
    </div> <!-- end of text-container -->
  </div> <!-- end of col -->
  <div class="col-lg-6">
    <div class="image-container">
      <div id="myCarousel" class="carousel slide" data-ride="carousel">

        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
          <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <div class="carousel-inner" role="listbox">
          <div class="item active">
            <img src="{{asset('asset/img/0.jpg')}}">
          </div>
          <div class="item">
            <img src="{{asset('asset/img/1.jpg')}}">
          </div>
          <div class="item">
            <img src="{{asset('asset/img/2.jpg')}}">
          </div>
        </div>

        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div> <!-- end of image-container -->
  </div> <!-- end of col -->
</div> <!-- end of row -->
<!-- end of header -->

</html>

@endsection