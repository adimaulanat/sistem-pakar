@extends ('layouts.layout')

@section ('content')

@if (session()->has('flash_notification.success'))
<div class="alert alert-success">{!! session('flash_notification.success') !!}</div>
@endif

<div class="col-md-12" style="padding:20px;">
    <br />
    <br />
    <div class="table-responsive">
        <table class="table table-striped">
            <thead class="table hover">
                <tr>
                    <th>NO</th>
                    <th>Nama - Posisi</th>
                    <th>Tanggal Akses</th>
                    <th>Status</th>
                    <th>Petugas</th>
                    <th>Kesimpulan</th>
                    @if(strtolower(Auth::user()->jabatan) == 'section head')
                    <th>Action</th>
                    @endif
                </tr>
            </thead>
            @foreach($history as $key=>$h)
            <tbody>

            </tbody>
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $h->nama }}</td>
                <td>{{ \Carbon\Carbon::parse($h->created_at)->format('d M, Y')}}</td>
                <td>{{ $h->status }}</td>
                <td>{{ $h->petugas }}</td>
                <td>{{ $h->kesimpulan }}</td>
                @if(strtolower(Auth::user()->jabatan) == 'section head')
                <td>
                    @if ($h->status != 'DISETUJUI' && $h->status != 'DITOLAK')
                    <a style="color: green;" href="/history/approve/{{ $h->id }}/{{Auth::user()->name}}">Setujui</a>
                    |
                    <a style="color: red;" href="/history/reject/{{ $h->id }}/{{Auth::user()->name}}">Tolak</a>
                    @else
                    <a style="color: yellow;">Tidak ada aksi</a>
                    @endif
                </td>
                @endif
            </tr>
            @endforeach
        </table>
        <div class="form-group row mb-0">
            <div class="col-md-8">
                <a href="/history/export" class="btn btn-success">
                    Export Excel
                </a>
            </div>
        </div>
        {{$history -> links()}}
    </div>
</div>
@endsection