@extends ('layouts.layout')

@section ('content')
<div id="contact" class="form-2">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2>Contact Information</h2>
                <ul class="list-unstyled li-space-lg">
                    <li class="address"><a class="turquoise">Jalan Raya Cimareme no. 275</a></li>
                    <li><i class="fas fa-phone"></i><a class="turquoise">+62 895-3449-80599</a></li>
                    <li><i class="fas fa-envelope"></i><a class="turquoise" href="mailto:denaldieza07@gmail.com">denaldieza07@gmail.com</a></li>
                </ul>
            </div> <!-- end of col -->
        </div> <!-- end of row -->
    </div> <!-- end of container -->
</div> <!-- end of form-2 -->
<!-- end of contact -->
@endsection