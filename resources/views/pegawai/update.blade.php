@extends ('layouts.layout')

@section ('content')
<div class="col-md-12" style="padding:20px;">
    @foreach($users as $p)
    <form class="form-horizontal" action="/pegawai/update" method="post">
        {{ csrf_field() }}
        <!-- <input type="hidden" name="_token" value="{{ csrf_token() }}"> -->
        <input type="hidden" name="id" value="{{ $p->id }}">
        <div class="form-group">
            <label class="control-label col-sm-2" for="name">Nama Pegawai:</label>
            <div class="col-sm-10">
                <input id="name" type="name" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $p->name }}" required autocomplete="nama_pegawai" autofocus>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="no_scan">e-mail</label>
            <div class="col-sm-10">
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $p->email }}" required autocomplete="email" autofocus>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="no_scan">No. Scan:</label>
            <div class="col-sm-10">
                <input id="no_scan" type="no_scan" class="form-control @error('no_scan') is-invalid @enderror" name="no_scan" value="{{ $p->no_scan }}" required autocomplete="no_scan" autofocus>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="nik">NIK:</label>
            <div class="col-sm-10">
                <input id="nik" type="nik" class="form-control @error('nik') is-invalid @enderror" name="nik" value="{{ $p->nik }}" required autocomplete="nik" autofocus>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="jabatan">Jabatan:</label>
            <div class="col-sm-10">
                <select name="jabatan">
                    @foreach($jabatan as $k)
                    <option {{ ( $p->jabatan == $k->name ) ? 'selected' : '' }} name="jabatan" value="{{$k->name}}">{{ $k->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="password">Password:</label>
            <div class="col-sm-10">
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" value="{{ old('password') }}" required autocomplete="password" autofocus>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary btn-lg">Submit</button>
            </div>
        </div>
    </form>
    @endforeach
</div>
@endsection