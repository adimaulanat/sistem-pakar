@extends ('layouts.layout')

@section ('content')

@if (session()->has('flash_notification.success'))
<div class="alert alert-success">{!! session('flash_notification.success') !!}</div>
@endif

<div class="col-md-12">
  <!-- <a href="/pertanyaan/tambah"> + Tambah Pertanyaan Baru</a> -->
  @include('modal.tambah_pegawai')
  <br />
  <br />
  <div class="table-responsive">
    <table class="table table-striped">
      <thead class="table hover">
        <tr>
          <th>NO</th>
          <th>Nama Pegawai</th>
          <th>E-mail</th>
          <th>No.Scan</th>
          <th>NIK</th>
          <th>Jabatan</th>
          @if(Auth::user()->hasRole('admin'))
          <th>Aksi</th>
          @endif
        </tr>
      </thead>
      @foreach($pegawai as $key=>$p)
      <tbody>

      </tbody>
      <tr>
        <td>{{ $key+1 }}</td>
        <td>{{ $p->name }}</td>
        <td>{{ $p->email }}</td>
        <td>{{ $p->no_scan }}</td>
        <td>{{ $p->nik }}</td>
        <td> {{ $p->jabatan }} </td>
        @if(Auth::user()->hasRole('admin'))
        <td>
          <a style="color: green;" href="/pegawai/update/{{ $p->id }}">Edit</a>
          |
          <a style="color: red;" href="/pegawai/delete/{{ $p->id }}">Hapus</a>
        </td>
        @endif
      </tr>
      @endforeach
    </table>

    {{$pegawai -> links()}}
  </div>
</div>
@endsection