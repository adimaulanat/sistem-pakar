<div id="ModalAddPegawaiForm" class="modal fade">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title">Tambah Pegawai Baru</h1>
            </div>
            <div class="modal-body">
                <form role="form" method="POST" action="/pegawai/store">
                    {{ csrf_field() }}
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label class="control-label">Nama Pegawai</label>
                        <div>
                            <input id="nama_pegawai" type="nama_pegawai" class="form-control @error('nama_pegawai') is-invalid @enderror" name="nama_pegawai" value="{{ old('nama_pegawai') }}" required autocomplete="nama_pegawai" autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">No. Scan</label>
                        <div>
                            <input id="no_scan" type="no_scan" class="form-control @error('no_scan') is-invalid @enderror" name="no_scan" value="{{ old('no_scan') }}" required autocomplete="no_scan" autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">NIK</label>
                        <div>
                            <input id="nik" type="nik" class="form-control @error('nik') is-invalid @enderror" name="nik" value="{{ old('nik') }}" required autocomplete="nik" autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <div>
                            <label class="control-label">Jabatan</label>
                            <div>
                                <select name="jabatan">
                                    <option name="jabatan" value="Section Head"> Section Head</option>
                                    <option name="jabatan" value="Group Leader"> Group Leader</option>
                                    <option name="jabatan" value="Admin"> Admin</option>
                                    <option name="jabatan" value="Operator"> Operator</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Password</label>
                        <div>
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" value="{{ old('password') }}" required autocomplete="password" autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <div>
                            <button type="submit" class="btn btn-success">SUBMIT</button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->