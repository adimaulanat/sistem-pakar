<div id="ModalAddForm" class="modal fade">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title">Tambah Pertanyaan Baru</h1>
            </div>
            <div class="modal-body">
                <form role="form" method="POST" action="/pengetahuan/store">
                    {{ csrf_field() }}
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label class="control-label">Pertanyaan</label>
                        <div>
                            <input type="pertanyaan" class="form-control input-lg" name="pertanyaan" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <div>
                            <label class="control-label">Jawaban</label>
                            <div class="checkbox">
                                <input style="font-size: 16px" type="radio" name="jawaban" value="1"><label>YA</label><br>
                                <input style="font-size: 16px" type="radio" name="jawaban" value="0"><label>TIDAK</label><br>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Solusi</label>
                        <div>
                            <input type="solusi" class="form-control input-lg" name="solusi" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Kategori</label>
                        <div>
                            <select name="kategori_id">
                                <option name="kategori_id" value="0">Warna dan Corak</option>
                                <option name="kategori_id" value="1">Benang</option>
                                <option name="kategori_id" value="2">Kualitas</option>
                                <option name="kategori_id" value="3">Konstruksi pada Benang</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div>
                            <button type="submit" class="btn btn-success btn-lg">SUBMIT</button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->