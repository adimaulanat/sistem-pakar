@extends ('layouts.layout')

@section ('content')
<!DOCTYPE html>
<html lang="en">

<head>
  <title>TENTANG APLIKASI DAN QC</title>
</head>

<body>
  <div class="col-md-12" style="padding:20px;">
    <div class="turquoise">
      <h1><span class="turquoise">TENTANG APLIKASI</span></h1>
      <h4>Aplikasi quality control merupakan sebuah aplikasi yang dapat membantu karyawan QC dalam
        menentukan bahan baku yang diperiksanya termasuk Passed atau Reject. Penentuan tersebut
        berdasarkan kepada hasil memilih pertanyaan yang diinputkan oleh pengguna. Metode inferensi quality
        yang digunakan untuk menentukan kategori dari bahan baku tersebut adalah menggunakan metode
        forward chaining. Aplikasi ini bertujuan untuk memudahkan karyawan QC dalam menentukan bahan
        baku yang diperiksanya termasuk passed atau reject. Dengan adanya aplikasi quality control
        menggunakan metode forward chaining berbasis web ini, diharapkan pengguna dapat mengetahui
        dengan mudah tentang penentuan kategori bahan baku yang sedang diperiksa.</p>
        <br />
        <br />
        <h1><span class="turquoise">TENTANG QC</span></h1>
        <h4>Quality Control atau pengendalian kualitas adalah suatu usaha untuk mempertahankan mutu atau
          kualitas dari barang yang dihasilkan agar sesuai dengan spesifikasi produk yang telah ditetapkan
          berdasarkan kebijakan pimpinan perusahaan. Dapat disimpulkan juga bahwa pengendalian kualitas
          adalah suatu kegiatan atau usaha yang dilakukan dalam rangka mencegah terjadinya kerusakan atau
          ketidaksesuaian kualitas sebagiamana mestinya yang telah ditetapkan. Adanya pengendalian kualitas
          diharapkan perusahaan dapat meminimalisir terjadinya produk cacat diluar batas yang diinginkan,
          sehingga perusahaan juga dapat mempertahankan kualitas dari produk yang dihasilkan.</p>
    </div>
  </div>
</body>

</html>

@endsection