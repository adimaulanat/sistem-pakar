@extends ('layouts.layout')

@section ('content')

@if (session()->has('flash_notification.success'))
<div class="alert alert-success">{!! session('flash_notification.success') !!}</div>
@endif

<div class="col-md-12" style="padding:20px;">
  <!-- <a href="/pertanyaan/tambah"> + Tambah Pertanyaan Baru</a> -->
  <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#ModalAddForm">
    Tambah Pertanyaan Baru
  </button>
  @include('modal.tambah')
  <br />
  <br />
  <div class="table-responsive">
    <table class="table table-striped">
      <thead class="table hover">
        <tr>
          <th>NO</th>
          <th>Pertanyaan</th>
          <th>Solusi</th>
          <th>Kategori</th>
          <th>Aksi</th>
        </tr>
      </thead>
      @foreach($pertanyaan as $key=>$p)
      <tbody>

      </tbody>
      <tr>
        <td>{{ $key+1 }}</td>
        <td>{{ $p->deskripsi }}</td>
        <td>{{ $p->solusi }}</td>
        <td>{{ $p->kategori->nama_kategori }}</td>
        <td>
          <a style="color: green;" href="/update/{{ $p->id }}">Edit</a>
          |
          <a style="color: red;" href="/pengetahuan/delete/{{ $p->id }}">Hapus</a>
        </td>
      </tr>
      @endforeach
    </table>
    {{ $pertanyaan -> links() }}
  </div>
</div>
@endsection