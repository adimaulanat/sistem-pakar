<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="keyword" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- OG Meta Tags to improve the way the post looks when you share the page on LinkedIn, Facebook, Google+ -->
    <meta property="og:site_name" content="" /> <!-- website name -->
    <meta property="og:site" content="" /> <!-- website link -->
    <meta property="og:title" content="" /> <!-- title shown in the actual shared post -->
    <meta property="og:description" content="" /> <!-- description shown in the actual shared post -->
    <meta property="og:image" content="" /> <!-- image link, make sure it's jpg -->
    <meta property="og:url" content="" /> <!-- where do you want your post to link to -->
    <meta property="og:type" content="article" />
    <title>Quality Control</title>

    <!-- start: Css -->
    <link rel="stylesheet" type="text/css" href="{{asset('asset/css/bootstrap.min.css')}}">

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,400i,600,700,700i&amp;subset=latin-ext" rel="stylesheet">
    <link href="{{asset('asset/evolvo/css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('asset/evolvo/css/fontawesome-all.css')}}" rel="stylesheet">
    <link href="{{asset('asset/evolvo/css/swiper.css')}}" rel="stylesheet">
    <link href="{{asset('asset/evolvo/css/magnific-popup.css')}}" rel="stylesheet">
    <link href="{{asset('asset/evolvo/css/styles.css')}}" rel="stylesheet">

    <!-- plugins -->
    <link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/font-awesome.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/simple-line-icons.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/animate.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/fullcalendar.min.css')}}" />
    <link href="{{asset('asset/css/style.css')}}" rel="stylesheet">
    <!-- end: Css -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css"> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

    <link rel="shortcut icon" href="asset/img/logomi.png">
    <script type="text/javascript" src="{{ URL::asset('js/jquery.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/bootstrap.js') }}"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body data-spy="scroll" data-target=".fixed-top">
    @include('layouts.navigation')

    <!-- start: content -->
    <!-- <div id="content"> -->
    <!-- Header -->
    <header id="header" class="header" style="height: max-content;">
        <div class="header-content">
            <div class="container">
                @yield('content')
            </div> <!-- end of container -->
        </div> <!-- end of header-content -->
    </header> <!-- end of header -->
    <!-- end of header -->

    <!-- Copyright -->
    <div class="copyright" style="position: fixed; bottom: 0; width: 100%;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright © Denaldi Eza Nuari</p>
                </div> <!-- end of col -->
            </div> <!-- enf of row -->
        </div> <!-- end of container -->
    </div> <!-- end of copyright -->
    <!-- end of copyright -->
    <!-- </div> -->
    <!-- end: content -->

    <!-- start: Javascript -->
    <script src="{{ URL::asset('asset/js/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('asset/js/jquery.ui.min.js') }}"></script>
    <script src="{{ URL::asset('asset/js/bootstrap.min.js') }}"></script>

    <!-- Scripts Evolvo-->
    <script src="{{ URL::asset('asset/evolvo/js/jquery.min.js')}}"></script> <!-- jQuery for Bootstrap's JavaScript plugins -->
    <script src="{{ URL::asset('asset/evolvo/js/popper.min.js') }}"></script> <!-- Popper tooltip library for Bootstrap -->
    <script src="{{ URL::asset('asset/evolvo/js/bootstrap.min.js') }}"></script> <!-- Bootstrap framework -->
    <script src="{{ URL::asset('asset/evolvo/js/jquery.easing.min.js') }}"></script> <!-- jQuery Easing for smooth scrolling between anchors -->
    <script src="{{ URL::asset('asset/evolvo/js/swiper.min.js') }}"></script> <!-- Swiper for image and text sliders -->
    <script src="{{ URL::asset('asset/evolvo/js/jquery.magnific-popup.js') }}"></script> <!-- Magnific Popup for lightboxes -->
    <script src="{{ URL::asset('asset/evolvo/js/validator.min.js') }}"></script> <!-- Validator.js - Bootstrap plugin that validates forms -->
    <script src="{{ URL::asset('asset/evolvo/js/scripts.js') }}"></script> <!-- Custom scripts -->

    <!-- plugins -->
    <script src="{{ URL::asset('asset/js/plugins/moment.min.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/fullcalendar.min.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/jquery.nicescroll.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/jquery.vmap.min.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/maps/jquery.vmap.world.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/jquery.vmap.sampledata.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/chart.min.js') }}"></script>


    <!-- custom -->
    <script src="{{ URL::asset('asset/js/main.js') }}"></script>
    <script type="text/javascript">
        (function(jQuery) {

            // start: Chart =============

            Chart.defaults.global.pointHitDetectionRadius = 1;
            Chart.defaults.global.customTooltips = function(tooltip) {

                var tooltipEl = $('#chartjs-tooltip');

                if (!tooltip) {
                    tooltipEl.css({
                        opacity: 0
                    });
                    return;
                }

                tooltipEl.removeClass('above below');
                tooltipEl.addClass(tooltip.yAlign);

                var innerHtml = '';
                if (undefined !== tooltip.labels && tooltip.labels.length) {
                    for (var i = tooltip.labels.length - 1; i >= 0; i--) {
                        innerHtml += [
                            '<div class="chartjs-tooltip-section">',
                            '   <span class="chartjs-tooltip-key" style="background-color:' + tooltip.legendColors[i].fill + '"></span>',
                            '   <span class="chartjs-tooltip-value">' + tooltip.labels[i] + '</span>',
                            '</div>'
                        ].join('');
                    }
                    tooltipEl.html(innerHtml);
                }

                tooltipEl.css({
                    opacity: 1,
                    left: tooltip.chart.canvas.offsetLeft + tooltip.x + 'px',
                    top: tooltip.chart.canvas.offsetTop + tooltip.y + 'px',
                    fontFamily: tooltip.fontFamily,
                    fontSize: tooltip.fontSize,
                    fontStyle: tooltip.fontStyle
                });
            };
            var randomScalingFactor = function() {
                return Math.round(Math.random() * 100);
            };
            var lineChartData = {
                labels: ["January", "February", "March", "April", "May", "June", "July"],
                datasets: [{
                    label: "My First dataset",
                    fillColor: "rgba(21,186,103,0.4)",
                    strokeColor: "rgba(220,220,220,1)",
                    pointColor: "rgba(66,69,67,0.3)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(220,220,220,1)",
                    data: [18, 9, 5, 7, 4.5, 4, 5, 4.5, 6, 5.6, 7.5]
                }, {
                    label: "My Second dataset",
                    fillColor: "rgba(21,113,186,0.5)",
                    strokeColor: "rgba(151,187,205,1)",
                    pointColor: "rgba(151,187,205,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: [4, 7, 5, 7, 4.5, 4, 5, 4.5, 6, 5.6, 7.5]
                }]
            };

            var doughnutData = [{
                    value: 300,
                    color: "#129352",
                    highlight: "#15BA67",
                    label: "Alfa"
                },
                {
                    value: 50,
                    color: "#1AD576",
                    highlight: "#15BA67",
                    label: "Beta"
                },
                {
                    value: 100,
                    color: "#FDB45C",
                    highlight: "#15BA67",
                    label: "Gamma"
                },
                {
                    value: 40,
                    color: "#0F5E36",
                    highlight: "#15BA67",
                    label: "Peta"
                },
                {
                    value: 120,
                    color: "#15A65D",
                    highlight: "#15BA67",
                    label: "X"
                }

            ];


            var doughnutData2 = [{
                    value: 100,
                    color: "#129352",
                    highlight: "#15BA67",
                    label: "Alfa"
                },
                {
                    value: 250,
                    color: "#FF6656",
                    highlight: "#FF6656",
                    label: "Beta"
                },
                {
                    value: 100,
                    color: "#FDB45C",
                    highlight: "#15BA67",
                    label: "Gamma"
                },
                {
                    value: 40,
                    color: "#FD786A",
                    highlight: "#15BA67",
                    label: "Peta"
                },
                {
                    value: 120,
                    color: "#15A65D",
                    highlight: "#15BA67",
                    label: "X"
                }

            ];

            var barChartData = {
                labels: ["January", "February", "March", "April", "May", "June", "July"],
                datasets: [{
                        label: "My First dataset",
                        fillColor: "rgba(21,186,103,0.4)",
                        strokeColor: "rgba(220,220,220,0.8)",
                        highlightFill: "rgba(21,186,103,0.2)",
                        highlightStroke: "rgba(21,186,103,0.2)",
                        data: [65, 59, 80, 81, 56, 55, 40]
                    },
                    {
                        label: "My Second dataset",
                        fillColor: "rgba(21,113,186,0.5)",
                        strokeColor: "rgba(151,187,205,0.8)",
                        highlightFill: "rgba(21,113,186,0.2)",
                        highlightStroke: "rgba(21,113,186,0.2)",
                        data: [28, 48, 40, 19, 86, 27, 90]
                    }
                ]
            };

            window.onload = function() {
                var ctx = $(".doughnut-chart")[0].getContext("2d");
                window.myDoughnut = new Chart(ctx).Doughnut(doughnutData, {
                    responsive: true,
                    showTooltips: true
                });

                var ctx2 = $(".line-chart")[0].getContext("2d");
                window.myLine = new Chart(ctx2).Line(lineChartData, {
                    responsive: true,
                    showTooltips: true,
                    multiTooltipTemplate: "<%= value %>",
                    maintainAspectRatio: false
                });

                var ctx3 = $(".bar-chart")[0].getContext("2d");
                window.myLine = new Chart(ctx3).Bar(barChartData, {
                    responsive: true,
                    showTooltips: true
                });

                var ctx4 = $(".doughnut-chart2")[0].getContext("2d");
                window.myDoughnut2 = new Chart(ctx4).Doughnut(doughnutData2, {
                    responsive: true,
                    showTooltips: true
                });

            };

            //  end:  Chart =============

            // start: Calendar =========
            $('.dashboard .calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                defaultDate: '2015-02-12',
                businessHours: true, // display business hours
                editable: true,
                events: [{
                        title: 'Business Lunch',
                        start: '2015-02-03T13:00:00',
                        constraint: 'businessHours'
                    },
                    {
                        title: 'Meeting',
                        start: '2015-02-13T11:00:00',
                        constraint: 'availableForMeeting', // defined below
                        color: '#20C572'
                    },
                    {
                        title: 'Conference',
                        start: '2015-02-18',
                        end: '2015-02-20'
                    },
                    {
                        title: 'Party',
                        start: '2015-02-29T20:00:00'
                    },

                    // areas where "Meeting" must be dropped
                    {
                        id: 'availableForMeeting',
                        start: '2015-02-11T10:00:00',
                        end: '2015-02-11T16:00:00',
                        rendering: 'background'
                    },
                    {
                        id: 'availableForMeeting',
                        start: '2015-02-13T10:00:00',
                        end: '2015-02-13T16:00:00',
                        rendering: 'background'
                    },

                    // red areas where no events can be dropped
                    {
                        start: '2015-02-24',
                        end: '2015-02-28',
                        overlap: false,
                        rendering: 'background',
                        color: '#FF6656'
                    },
                    {
                        start: '2015-02-06',
                        end: '2015-02-08',
                        overlap: true,
                        rendering: 'background',
                        color: '#FF6656'
                    }
                ]
            });
            // end : Calendar==========

            // start: Maps============

            jQuery('.maps').vectorMap({
                map: 'world_en',
                backgroundColor: null,
                color: '#fff',
                hoverOpacity: 0.7,
                selectedColor: '#666666',
                enableZoom: true,
                showTooltip: true,
                values: sample_data,
                scaleColors: ['#C8EEFF', '#006491'],
                normalizeFunction: 'polynomial'
            });

            // end: Maps==============

        })(jQuery);
    </script>
    <!-- end: Javascript -->
</body>

</html>