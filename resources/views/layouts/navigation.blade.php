<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark navbar-custom fixed-top" style="box-shadow: 0 0.0625rem 0.375rem 0 rgba(0, 0, 0, 0.1);">
    <!-- Text Logo - Use this if you don't have a graphic logo -->
    <!-- <a class="navbar-brand logo-text page-scroll" href="index.html">Evolo</a> -->

    <!-- Image Logo -->
    <a class="navbar-brand logo-image" href="/beranda"><img src="{{asset('asset/img/logo.jpg')}}" alt="alternative" style="padding: 0; width: 20%; height: 100%;"></a>
    <a class="navbar-brand" style="height: 0; color: black !important; text-shadow:0px 0px 0px ; position:absolute; left: 20%"> Quality Control Apps </a>
    <button href="javascript:void(0);" onclick="myFunction()" class="navbar-toggler" type="button">
        <span class="navbar-toggler-awesome fas fa-bars"></span>
    </button>
    <!-- Mobile Menu Toggle Button -->
    <!-- end of mobile menu toggle button -->

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav ml-auto" style="margin-right: 30px">
            @if(Auth::user()->hasRole('employee'))
            <li class="dropdown avatar-dropdown">
                <a style="color: black" class="nav-link page-scroll" href="/beranda">Beranda</a>
            </li>
            <li class="dropdown avatar-dropdown">
                <a style="color: black" class="nav-link dropdown-toggle page-scroll" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Konsultasi</a>
                <ul class="dropdown-menu user-dropdown">
                    <li><a style="color: black" class="dropdown-item" href="/konsultasi">Isi Konsultasi</a></li>
                    <div class="dropdown-items-divide-hr"></div>
                    <li><a style="color: black" class="dropdown-item" href="/history">Report</a></li>
                </ul>
            </li>
            <li class="dropdown avatar-dropdown">
                <a style="color: black" class="nav-link dropdown-toggle page-scroll" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Informasi</a>
                <ul class="dropdown-menu user-dropdown">
                    <li><a style="color: black" class="dropdown-item" href="/tentang">Tentang Aplikasi</a>
                        <div class="dropdown-items-divide-hr"></div>
                    <li><a style="color: black" class="dropdown-item" href="/pegawai">Data Pegawai</a>
                </ul>
            </li>
            <li class="dropdown avatar-dropdown">
                <a style="color: black" class="nav-link page-scroll" href="/contact">Contact</a>
            </li>
            <!-- end of dropdown menu -->
        </ul>
        @else
        <!-- Dropdown Menu -->
        <li class="nav-item dropdown user-name">
            <a class="nav-link dropdown-toggle page-scroll" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Menu</a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="/pengetahuan">Pengetahuan</a>
                <div class="dropdown-items-divide-hr"></div>
                <a class="dropdown-item" href="/pegawai">Pegawai</a>
            </div>
        </li>
        <li class="nav-item dropdown user-name">
            <a class="nav-link page-scroll">{{ Auth::user()->name }}<span class="sr-only">(current)</span></a>
        </li>

        @endif


        <!-- Dropdown Menu -->
        <li class="nav-item dropdown avatar-dropdown">
        <li class="dropdown avatar-dropdown">
            <img src="{{asset('asset/img/avatar.jpg')}}" class="img-circle avatar" alt="user name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" />
            <ul class="dropdown-menu user-dropdown">
                <li>
                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                           document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
        </li>
        </li>
        <!-- end of dropdown menu -->
        </ul>
    </div>
</nav> <!-- end of navbar -->
<!-- end of navigation -->

<script type="text/javascript">
    function myFunction() {
        var x = document.getElementById("navbarsExampleDefault");
        if (x.style.display === "block") {
            x.style.display = "none";
        } else {
            x.style.display = "block";
            x.style.backgroundColor = "White";
        }
    }
</script>