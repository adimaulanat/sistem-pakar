<!-- start: Header -->
<nav class="navbar navbar-default header navbar-fixed-top">
  <div class="col-md-12 nav-wrapper">
    <div class="navbar-header" style="width:100%;">

      <ul class="nav navbar-nav navbar-right user-nav">
        @if(Auth::user()->hasRole('employee'))
        <li class="ripple">
          <a href="/login">
            <span class="fa fa-user"></span> Login User
          </a>
        </li>
        @else
        <li class="user-name"><span>{{ Auth::user()->name }}</span></li>
        <li class="dropdown avatar-dropdown">
          <img src="{{asset('asset/img/avatar.jpg')}}" class="img-circle avatar" alt="user name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" />
          <ul class="dropdown-menu user-dropdown">
            <li>
              <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
              </a>

              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
              </form>
            </li>
          </ul>
        </li>
        @endguest
      </ul>
    </div>
  </div>
</nav>
<!-- end: Header