@extends ('layouts.layout')

@section ('content')
<div class="col-md-12" style="padding:20px;">
  <br />
  <br />
  <div class="row justify-content-center">
    <div class="col-md-8">
      @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
          <li>Jawaban harap diisi!</li>
          @endforeach
        </ul>
      </div>
      @endif
      <div class="card">
        <form class="form-horizontal" @if ($next) action="/konsultasi/pertanyaan/{{ $next->id }}" method="get" @else action="/konsultasi/solusi" method="get" @endif style="padding: 20px">
          {{ csrf_field() }}
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <input type="hidden" name="id" value="{{ $pertanyaan->id }}">
          <input type="hidden" name="solusi" value="{{ $pertanyaan->solusi }}">
          <input type="hidden" name="jawaban_id" value="{{ $pertanyaan->jawaban_id }}">

          <div class="jumbotron text-center" style="background-color: #01bfd9; color: white">
            <div class="form-group">
              <p style="line-height: 1.5;">{{ $pertanyaan->deskripsi }}</label>
            </div>
            <div class="form-group">
              <input type="radio" name="jawaban" value="1"> YA
              <input style="margin-left: 15px; margin-bottom: 20px" type="radio" name="jawaban" value="0"> TIDAK<br>
              @if($next)
              <button type="submit" class="btn btn-primary btn-lg">
                Submit Jawaban </button>
              @else
              <button type="submit" class="btn btn-primary btn-lg">
                Tarik Kesimpulan</button>
              @endif
            </div>
          </div>
        </form>
      </div>

    </div>
  </div>
</div>
@endsection