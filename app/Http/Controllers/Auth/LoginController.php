<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/home';
    // protected $redirectTo = '/dashboard';
    protected function authenticated(Request $request, $user)
    {
        if ($request->user()->hasRole('admin')) {
            $redirectTo = '/dashboard';
            $request->session()->flash('flash_notification.success', 'Congratulations, you has been logged in!');
            return redirect($redirectTo);
        }

        if ($request->user()->hasRole('employee')) {
            $history = DB::table('history')->where('petugas', 'Belum diproses petugas')->get();
            $redirectTo = '/beranda';
            $request->session()->flash('flash_notification.success', 'Congratulations, you has been logged in!');
            $request->session()->flash('flash_notification.history', 'Ada ' . count($history) . ' report yang belum disetujui');
            return redirect($redirectTo);
        }
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
