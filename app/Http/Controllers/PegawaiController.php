<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Pegawai;
use App\Jabatan;
use App\User;

class PegawaiController extends Controller
{
    public function index()
    {
        $pegawai = User::paginate(10);
        return view('pegawai.pegawai', ['pegawai' => $pegawai]);
    }

    public function store(Request $request)
    {

        // insert data ke table pertanyaan
        DB::table('users')->insert([
            'nama_pegawai' => $request->nama_pegawai,
            'no_scan' => $request->no_scan,
            'nik' => $request->nik,
            'jabatan' => $request->jabatan,
            'password' => Hash::make($request->password)
        ]);
        // alihkan halaman ke halaman pengetahuan
        return redirect('/pegawai');
    }

    public function update($id)
    {
        $pegawai = DB::table('users')->where('id', $id)->get();
        $jabatan = Jabatan::all();
        return view('pegawai.update', ['users' => $pegawai, 'jabatan' => $jabatan]);
    }

    public function postUpdate(Request $request)
    {
        // update data ke table pertanyaan
        DB::table('users')->where('id', $request->id)->update([
            'name' => $request->name,
            'email' => $request->email,
            'no_scan' => $request->no_scan,
            'nik' => $request->nik,
            'password' => Hash::make($request->password),
            'jabatan' => $request->jabatan
        ]);
        $request->session()->flash('flash_notification.success', 'Congratulations, data has been updated!');
        // alihkan halaman ke halaman pengetahuan
        return redirect('/pegawai');
    }

    public function delete($id, Request $request)
    {
        // menghapus data pertanyaan berdasarkan id yang dipilih
        DB::table('pegawai')->where('id_pegawai', $id)->delete();
        $request->session()->flash('flash_notification.success', 'Congratulations, data has been deleted!');
        // alihkan halaman ke halaman pegawai
        return redirect('/pegawai');
    }
}
