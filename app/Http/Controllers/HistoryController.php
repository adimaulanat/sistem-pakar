<?php

namespace App\Http\Controllers;

use App\Exports\HistoryExport;
use Illuminate\Http\Request;
use App\History;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class HistoryController extends Controller
{
    public function index()
    {
        $history = History::paginate(10);
        return view('history', ['history' => $history]);
    }

    public function approve(Request $request, $id, $nama)
    {
        DB::table('history')->where('id', $id)->update([
            'status' => 'DISETUJUI',
            'petugas' => $nama
        ]);
        $request->session()->flash('flash_notification.success', 'Data has been updated!');
        return redirect('/history');
    }

    public function reject(Request $request, $id, $nama)
    {
        DB::table('history')->where('id', $id)->update([
            'status' => 'DITOLAK',
            'petugas' => $nama
        ]);
        $request->session()->flash('flash_notification.success', 'Data has been updated!');
        return redirect('/history');
    }

    public function export()
    {
        return Excel::download(new HistoryExport, 'history.xlsx');
    }
}
