<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Pertanyaan;
use App\History;
use App\Pegawai;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;

class PertanyaanUserController extends Controller
{
    public $solusi = array();
    // public function guard()
    // {
    //     return Auth::guard('pegawai');
    // }

    public function pertanyaan()
    {
        $pertanyaan = Pertanyaan::find(0);
        $next = $pertanyaan->next();
        session()->put('solution', []);
        return view('pertanyaan', [
            'pertanyaan' => $pertanyaan,
            'next' => $next,
            'solusi' => session()->get('solution'),
            'isSaved' => session()->get('isSaved'),
            'error' => session()->get('error')
        ]);
    }

    public function show(Request $request, $id)
    {
        // get the current question
        $pertanyaan = Pertanyaan::find($id);
        $next = $pertanyaan->next();

        $validatedData = $request->validate([
            'jawaban' => 'required'
        ]);

        if ($request->jawaban != $request->jawaban_id && $validatedData) {
            session()->push('solution', $request->solusi);
        }

        return view('pertanyaan', [
            'pertanyaan' => $pertanyaan,
            'next' => $next,
            'solusi' => session()->get('solution'),
            'isSaved' => session()->get('isSaved')
        ]);
    }

    public function kesimpulan(Request $request)
    {
        if ($request->jawaban != $request->jawaban_id) {
            session()->push('solution', $request->solusi);
        }
        $arrSolusi = session()->get('solution');
        if (count($arrSolusi) == 0) {
            $arrSolusi = 'Kain tersebut masuk standar dan simpan pada “layout passed” yang telah disediakan.';
        } else {
            $arrSolusi = array_unique($arrSolusi);
            $arrSolusi = implode("\n", $arrSolusi);
        }
        $last = DB::table('history')->latest()->first();
        DB::table('history')->where('id', $last->id)->update([
            'status' => 'MENUGGU PERSETUJUAN',
            'kesimpulan' => $arrSolusi
        ]);
        session()->put('isSaved', false);
        return view('konsultasi.kesimpulan', ['solusi' => $arrSolusi]);
    }

    public function history()
    {

        $nama_pegawai = Auth::user()->name;

        History::create([
            'nama' => $nama_pegawai,
            'created_at' => Carbon::now(),
            'status' => 'TIDAK SELESAI',
            'petugas' => 'Belum diproses petugas',
            'kesimpulan' => 'BELUM ADA',
        ]);
        session()->put('isSaved', true);
        session()->put('error', false);

        return redirect('/konsultasi/pertanyaan');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'jawaban' => ['required']
        ]);
    }
}
