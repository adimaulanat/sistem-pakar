<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Pertanyaan;
use App\Kategori;

class PertanyaanController extends Controller
{
    public function index()
    {
        // mengambil data dari table pertanyaan
        $pertanyaan = Pertanyaan::paginate(10);
        // mengirim data pertanyaan ke view index
        return view('pengetahuan', ['pertanyaan' => $pertanyaan]);
    }

    public function store(Request $request)
    {
        // insert data ke table pertanyaan
        DB::table('pertanyaan')->insert([
            'deskripsi' => $request->pertanyaan,
            'jawaban_id' => $request->jawaban,
            'solusi' => $request->solusi,
            'kategori_id' => $request->kategori_id
        ]);
        $request->session()->flash('flash_notification.success', 'Congratulations, data has been stored!');
        // alihkan halaman ke halaman pengetahuan
        return redirect('/pengetahuan');
    }

    public function update($id)
    {
        $pertanyaan = DB::table('pertanyaan')->where('id', $id)->get();
        $kategori = Kategori::all();
        return view('update', ['pertanyaan' => $pertanyaan, 'kategori' => $kategori]);
    }

    public function postUpdate(Request $request)
    {
        // update data ke table pertanyaan
        DB::table('pertanyaan')->where('id', $request->id)->update([
            'deskripsi' => $request->pertanyaan,
            'jawaban_id' => $request->jawaban,
            'solusi' => $request->solusi,
            'kategori_id' => $request->kategori_id
        ]);
        $request->session()->flash('flash_notification.success', 'Congratulations, data has been updated!');
        // alihkan halaman ke halaman pengetahuan
        return redirect('/pengetahuan');
    }

    public function delete($id, Request $request)
    {
        // menghapus data pertanyaan berdasarkan id yang dipilih
        DB::table('pertanyaan')->where('id', $id)->delete();
        $request->session()->flash('flash_notification.success', 'Congratulations, data has been deleted!');
        // alihkan halaman ke halaman pegawai
        return redirect('/pengetahuan');
    }
}
