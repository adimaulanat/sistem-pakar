<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jabatan extends Model
{
    protected $table = "jabatan";
    protected $fillable = ['name'];
    public function pegawais()
    {
        return $this->hasMany('App\Pegawai');
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
