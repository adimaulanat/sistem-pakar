<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Pegawai extends Authenticatable
{
    use Notifiable;
    protected $table = "pegawai";

    protected $fillable = [
        'nama_pegawai', 'password', 'no_scan', 'nik', 'jabatan'
    ];

    protected $hidden = [
        'id_pegawai', 'password'
    ];

    public function jabatan()
    {
        return $this->belongsTo('App\Jabatan');
    }

    // public function getPasswordPegawai()
    // {
    //     return $this->password;
    // }
}
