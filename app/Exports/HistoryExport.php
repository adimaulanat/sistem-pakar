<?php

namespace App\Exports;

use App\History;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class HistoryExport implements FromCollection, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return History::all();
    }

    public function headings(): array
    {
        return [
            'NO',
            'User',
            'Stauts',
            'Petugas',
            'Kesimpulan',
            'Tanggal Akses',
            'Tanggal Dibuat'
        ];
    }
}
