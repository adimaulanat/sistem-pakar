<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table = "kategori";

    public function kategori_pertanyaans()
    {
        return $this->hasMany('App\Pertanyaan');
    }
}
