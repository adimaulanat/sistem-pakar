<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    protected $table = "history";
    protected $fillable = ['nama', 'posisi', 'status', 'petugas', 'kesimpulan', 'created_at'];
}
