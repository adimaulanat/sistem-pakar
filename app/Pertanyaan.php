<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    protected $table = "pertanyaan";

    public function next()
    {
        return Pertanyaan::where('id', '>', $this->id)->orderBy('id', 'asc')->first();
    }

    public function previous()
    {
        return Pertanyaan::where('id', '<', $this->id)->orderBy('id', 'desc')->first();
    }

    public function jawaban()
    {
        return $this->belongsTo('App\Jawaban');
    }

    public function kategori()
    {
        return $this->belongsTo('App\Kategori');
    }
}
