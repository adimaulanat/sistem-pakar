<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jawaban extends Model
{
    protected $table = "jawaban";

    public function jawaban_pertanyaans()
    {
        return $this->hasMany('App\Pertanyaan');
    }
}
